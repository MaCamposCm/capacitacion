

CREATE TABLE USUARIO (
    ID_USUARIO NUMBER(5) NOT NULL,
    CLAVE  VARCHAR2(50),
    CORREO VARCHAR2(50) ,
    NOMBRE VARCHAR2(50) NOT NULL,
    AP_PATERNO VARCHAR2(50) NOT NULL,
    AP_MATERNO VARCHAR2(50),
    NOMBRE_COMPLETO VARCHAR2(150) NOT NULL,
    TELEFONO VARCHAR2(10),
    CONTRASENA VARCHAR2(36),
    CONSTRAINT PK_USUARIO PRIMARY KEY (ID_USUARIO),
    CONSTRAINT UC_USUARIO UNIQUE (CORREO)
);

CREATE TABLE ROL(
	ID_ROL NUMBER(5) NOT NULL,
	TIPO_ROL VARCHAR2(30),
	DESCRIPCION_ROL VARCHAR2(30),
	CONSTRAINT PK_ROL PRIMARY KEY (ID_ROL)
);

CREATE TABLE ROL_USUARIO(
	ID_ROL NUMBER(5) NOT NULL,
    ID_USUARIO NUMBER(5) NOT NULL,
	CONSTRAINT FK_RU_ROL FOREIGN KEY (ID_ROL) REFERENCES ROL(ID_ROL),
	CONSTRAINT FK_RU_USUARIO FOREIGN KEY (ID_USUARIO) REFERENCES USUARIO(ID_USUARIO)
);


CREATE TABLE MAESTRO(
	ID_MAESTRO NUMBER(5) ,
	ID_USUARIO NUMBER(5),
	CONSTRAINT PK_MAESTRO PRIMARY KEY (ID_MAESTRO),
	CONSTRAINT FK_MA_USUARIO FOREIGN KEY (ID_USUARIO) REFERENCES USUARIO(ID_USUARIO)
);

CREATE TABLE ALUMNO(
	ID_ALUMNO NUMBER(5),
	ID_USUARIO NUMBER(5),
	CONSTRAINT PK_ALUMNO PRIMARY KEY (ID_ALUMNO),
	CONSTRAINT FK_AL_USUARIO FOREIGN KEY (ID_USUARIO) REFERENCES USUARIO(ID_USUARIO)
);


CREATE TABLE MATERIA(
 	ID_MATERIA NUMBER(5) NOT NULL,
 	NOM_MATERIA VARCHAR2(20),
 	CONSTRAINT PK_MATERIA PRIMARY KEY (ID_MATERIA)
);

CREATE TABLE MAESTRO_MATERIA(
	ID_MAESTRO_MATERIA NUMBER(5),
	ID_MAESTRO NUMBER(5),
	ID_MATERIA NUMBER(5),
	CONSTRAINT PK_MAESTRO_MATERIA PRIMARY KEY (ID_MAESTRO_MATERIA),
	CONSTRAINT FK_MAMA_MAESTRO FOREIGN KEY (ID_MAESTRO) REFERENCES MAESTRO(ID_MAESTRO),
	CONSTRAINT FK_MAMA_MATERIA FOREIGN KEY (ID_MATERIA) REFERENCES MATERIA(ID_MATERIA)
);


CREATE TABLE ALUMNO_MATERIA(
	ID_ALUMNO_MATERIA NUMBER(5),
	ID_ALUMNO NUMBER(5),
	ID_MATERIA NUMBER(5),
	CONSTRAINT PK_ALUMNO_MATERIA PRIMARY KEY (ID_ALUMNO_MATERIA),
	CONSTRAINT FK_ALMA_MAESTRO FOREIGN KEY (ID_ALUMNO) REFERENCES ALUMNO(ID_ALUMNO),
	CONSTRAINT FK_ALMA_MATERIA FOREIGN KEY (ID_MATERIA) REFERENCES MATERIA(ID_MATERIA)
);

CREATE TABLE ASISTENCIA(
 	ID_ASISTENCIA NUMBER(5) NOT NULL,
 	FECHA DATE,
 	ID_MATERIA NUMBER(5),
 	CONSTRAINT PK_ASISTENCIA PRIMARY KEY (ID_ASISTENCIA),
 	CONSTRAINT FK_MATERIA_ASIS FOREIGN KEY (ID_MATERIA) REFERENCES MATERIA(ID_MATERIA)
);

CREATE TABLE ASISTENCIA_ALUMNO( 
	ID_ASISTENCIA NUMBER(5),
	ID_ALUMNO NUMBER(5),
	CONSTRAINT FK_ASAL_ASIS FOREIGN KEY (ID_ASISTENCIA) REFERENCES ASISTENCIA(ID_ASISTENCIA),
 	CONSTRAINT FK_ASAL_AL FOREIGN KEY (ID_ALUMNO) REFERENCES ALUMNO(ID_ALUMNO)	
);

-- ====================================== SEQUENCES ======================================


CREATE SEQUENCE SEQ_USUARIO
START WITH 1
INCREMENT BY 1 NOCACHE NOCYCLE;

CREATE SEQUENCE SEQ_ROL
START WITH 1
INCREMENT BY 1 NOCACHE NOCYCLE;



CREATE SEQUENCE SEQ_MATERIA
START WITH 1
INCREMENT BY 1 NOCACHE NOCYCLE;

CREATE SEQUENCE SEQ_ALUMNO
START WITH 1
INCREMENT BY 1 NOCACHE NOCYCLE;

commit;