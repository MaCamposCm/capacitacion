
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<div class="container">
	<ul id="slideNavMain"
		class="sidenav blue-grey-text text-darken-4 collapsible collapsible-accordion expandable ">

		<c:if test="${usuarioAutenticado}">
							<li class="active" ><a
					class="collapsible-header waves-effect waves-light center-align subheader-role z-depth-1">
					<img
					id="imgHeader"
					src="<c:url value="/resources/img/uaqLogoWhite.png" />">
					
					</a>
					<div class="collapsible-body">
						<ul class="collapsible collapsible-accordion expandable">
						
							<sec:authorize access="hasAnyRole('ROLE_ADMINISTRADOR','ROLE_MAESTRO','ROLE_ALUMNO')">
							
							</sec:authorize>
							<sec:authorize access="hasAnyRole('ADMINISTRADOR')">
								<li><a class="waves-effect waves-blue" href="${contextRoleActive}usuarios">Usuarios<i class="material-icons left ">people</i></a></li>
							</sec:authorize>

						</ul>
					</div></li>
		

		</c:if>
	</ul>
</div>