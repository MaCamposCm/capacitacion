<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<header>
	<div class="navbar-fixed">
		<nav class="hide-on-small-only">
			<div class="nav-wrapper ">
				<div class="sidenav-trigger" data-target="slideNavMain">
										<sec:authorize access="isAuthenticated()" var="usuarioAuthenticado" />
						<c:if test="${usuarioAuthenticado}">
				
						<a id="btnLaunchNav" class="large" href="#"> <i class=" material-icons "
							title="Abrir o cerrar barra lateral">menu</i>
						</a>
					</c:if>
				</div>
				<a href="https://www.uaq.mx/" target="_blank"> <img
					id="imgHeader"
					src="<c:url value="/resources/img/uaqLogoWhite.png" />">
					
				</a> <a class="center brand-logo truncate btnGoHome" href="#">Capacitación</a>
				<c:choose>
					<c:when test="${usuarioAutenticado}">
						<c:choose>
							<c:when test="${session.usuario != null}">
								<a style="background-color: #6c4774 !important" class="right brand-option btn-floating btn-large blue hoverable sessionBtn z-depth-3" href="#" >${fn:substring(session.usuario.nombre, 0, 1)}${fn:substring(session.usuario.apellidoPaterno, 0, 1)}</a>
							</c:when>
							<c:otherwise>
								<a style="background-color: #6c4774 !important" class="right brand-option btn-floating btn-large blue hoverable sessionBtn z-depth-3" href="#" ><i class="material-icons">account_circle</i></a>
							</c:otherwise>
						</c:choose>
						<div class="menuEmergenteSession z-depth-1 hoverable center">
							<div class="menuEmergenteSession-shape"></div>
							<div class="menuEmergenteSession-container">
								<ul>
									<li>
										<div class="user-view">
		   									<a class="btn-floating btn-large  sessionBtn-x2" href="#" >${fn:substring(session.usuario.nombre, 0, 1)}${fn:substring(session.usuario.apellidoPaterno, 0, 1)}</a>
		   									<p class="left-align">${session.usuario.nombreCompleto}</p>
		   									<c:choose>
		   										<c:when test="${session.usuario.correo != null}">
		   											<p class="left-align emailType"><span class="email">${session.usuario.correo}</span></p>
		   										</c:when>
		   										<c:otherwise>
		   											<p class="left-align emailType"><span class="email">Sin correo electrónico</span></p>
		   										</c:otherwise>
		   									</c:choose>
		   								</div>
		   							</li>
								</ul>
							</div>
							<div class="menuEmergenteSession-footer">
								<ul>
									<sec:authorize access="hasAnyRole('ROLE_ADMINISTRADOR','ROLE_MAESTRO','ROLE_ALUMNO')">
										<li><a class="btn-large waves-effect waves-light btnPerfil" style="background-color: #6c4774 !important" href="${contextRoleActive}perfil">ver mi perfil</a></li>
									</sec:authorize>
									<li><a class="btn-large waves-effect waves-light btnCerrarSesion   red darken-4" onclick="cerrarSesion()">Cerrar Sesión</a></li>
								</ul>
							</div>
						</div>
					</c:when>
<%-- 					<c:otherwise> --%>
<%-- 						<a class="right brand-option hoverable" href="${contextPath}login" style="margin-left: 1rem; margin-right: 1rem;"><i class="material-icons">vpn_key</i></a> --%>
<%-- 					</c:otherwise> --%>
				</c:choose>
			</div>
			
		</nav>
		
	</div>
	
</header>