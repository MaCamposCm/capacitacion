$("#formLogin").validate({
	rules: {
		username : {
			required: function(element) {
				var $username = $("#username");
				
				if($username.val().trim().length == 0)
					$username.val('');
				return true;
			}
		},
		password : {
			required : true
		}
	},
	messages: {
		username: {
			required : "Ingrese su clave o correo"
		},
		password : {
			required : "Ingrese su nip o contraseña"
		} 
	},
	errorElement : 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error)
        
      } else {
        error.insertAfter(element);
      }
      var select = (element[0].localName);
      if(select === "select") {
    	  var $input = $(element).parent().find("input");
    		
    	  if(!($input.hasClass("error"))) 
    		$input.addClass("error");
      }
    },
	success: function(label) {
    	var select = (label.prev('select'));
		
		if(select[0] != undefined) {
			if(select[0].localName === 'select') {
				var $input = select.prev().prev().prev();
				$input.removeClass('error').addClass('validate valid');
			}
		}
		
        label.html("Correcto").addClass("checked");
    }
});