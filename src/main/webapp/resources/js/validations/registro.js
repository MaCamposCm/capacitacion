$.validator.addMethod('passwordIgual',function (value, element){
	var $passwordUno =  $("#passwordCon");
	var $passwordDos =  $("#password2Con");
	if($passwordUno.val() == $passwordDos.val()){
		return true
		
	}else{
		return false
	} 
	
	
},"La contraseña es diferente")


$("#formRegistro").validate({
	rules: {
		correoCon : {
			required: function(element) {
				var $correoCon = $("#correoCon");
				
				if($correoCon.val().trim().length == 0)
					$correoCon.val('');
				return true;
			},
			email : true,
		}, 
		passwordCon : {
			required: function(element) {
				var $passwordCon = $("#passwordCon");
				
				if($passwordCon.val().trim().length == 0)
					$passwordCon.val('');
				return true;
			}, 
	
		},
		password2Con : {
			required: function(element) {
				var $password2Con = $("#password2Con");
				
				if($password2Con.val().trim().length == 0)
					$password2Con.val('');
				return true;
			}, 	
			
		passwordIgual: true 
		}, 
	 nombreCon : {
			required: function(element) {
				var $nombreCon = $("#nombreCon");
				
				if($nombreCon.val().trim().length == 0)
					$nombreCon.val('');
				return true;
			}, 
		},
		apellidoPCon : {
			required: function(element) {
				var $apellidoPCon = $("#apellidoPCon");
				
				if($apellidoPCon.val().trim().length == 0)
					$apellidoPCon.val('');
				return true;
			},
		},
		apellidoMCon : {
			required: function(element) {
				var $apellidoMCon = $("#apellidoMCon");
				
				if($apellidoMCon.val().trim().length == 0)
					$apellidoMCon.val('');
				return true;
			},
		},
		
	},  
	messages: {
		correoCon: {
			required : "Ingrese su clave o correo"
		},
		passwordCon:{
			required : "Ingrese contraseña"
			
		},
		password2Con:{
			required : "Ingrese contraseña"
			
		}, 
		nombreCon:{
			required : "Ingrese nombre"
			
		}, 
		apellidoPCon:{
			required : "Ingrese apellido paterno"
			
		}, 
		apellidoMCon:{
			required : "Ingrese apellido materno"
			
		}, 
		 
	},
	errorElement : 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error)
        
      } else {
        error.insertAfter(element);
      }
      var select = (element[0].localName);
      if(select === "select") {
    	  var $input = $(element).parent().find("input");
    		
    	  if(!($input.hasClass("error"))) 
    		$input.addClass("error");
      }
    },
	success: function(label) {
    	var select = (label.prev('select'));
		
		if(select[0] != undefined) {
			if(select[0].localName === 'select') {
				var $input = select.prev().prev().prev();
				$input.removeClass('error').addClass('validate valid');
			}
		}
		
        label.html("Correcto").addClass("checked");
    }
});