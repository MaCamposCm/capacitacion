var $btnRegistro = $("#btnRegistro"),
$formRegistro = $("#formRegistro"),
  $username = $("#username"),
  $password = $("#password");

$btnRegistro.on('click',function(e){
	e.preventDefault();
	if ($formRegistro.valid()){
		$.ajax({
			type:'POST',
			url:CONTEXT_PATH + 'registroUsuario',
			data: $formRegistro.customSerialize(), 
			success: function(data){
				if(data!=null){
					
					$username.val(data.clave).parent().find('label').addClass('active');
					$password.val(data.contrasena).parent().find('label').addClass('active');
					
					document.getElementById("tab-login").setAttribute("class","tab");

					$("#tabs-swipe-account").tabs("select", ("tab-login"));
					
					  
				}
				
			}
			
		})
		
	} else {
		showWarningAlert("No está validado")
	}
	
	
})