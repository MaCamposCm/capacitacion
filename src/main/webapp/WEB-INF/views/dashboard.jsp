<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<!-- RESOURCES -->
<%@ include file="/resources/templates/fixed_resources.jsp"%>
</head>

<body>
	<!-- VALIDATIONS LOGIN -->


	<c:if test="${usuarioAutenticado}">
		
			<sec:authorize access="hasRole('ROLE_ADMINISTRADOR')">
			<c:redirect url="/admin/noticias" />
		</sec:authorize>
		<sec:authorize access="hasRole('ROLE_MAESTRO')">
			<c:redirect url="/maestro/inicio" />
		</sec:authorize>
		<sec:authorize access="hasRole('ROLE_ALUMNO')">
			<c:redirect url="/alumno/inicio" />
		</sec:authorize>
		
	</c:if>
</body>
</html>