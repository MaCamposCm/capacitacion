<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<!-- RESOURCES -->
<%@ include file="/resources/templates/fixed_resources.jsp"%>



<title>${contextTitle} - Examen</title>
</head>
<style>
div.ex3 {
	overflow: auto;
}
</style>
<body onload="nobackbutton();">

	<!-- HEARDER -->
	<%@ include file="/resources/templates/header.jsp"%>
	<!-- MAIN -->

	<main>
	
<div class="card col s12 m10 offset-m1 hoverable">
		<div class="card-content">
			<div><h2 class="center-align">Noticias</h2> </div>	
				<div><h3 class="center-align">UAQ</h3></div>
					<hr>
					<br class="hide-on-med-and-down">
						<div class="row col m12 l12">
							<div class="row">
								<div class="card-panel">
							<ul class="collapsible">
								<li class="active">
									<div class="collapsible-header" tabindex="0">
											<div class="collapsible-body" style="display: block;">
													<p  style="text-align:justify"> 
													Las ideas que comunica un texto est�n contenidas en lo que se suele 
													denominar �macroproposiciones�, unidades estructurales de nivel superior 
													o global, que otorgan coherencia al texto constituyendo su hilo central,
 													el esqueleto estructural que cohesiona elementos ling��sticos formales 
 													de alto nivel, como los t�tulos y subt�tulos, la secuencia de p�rrafos, 
 													etc. En contraste, las �microproposiciones� son los elementos coadyuvantes 
													de la cohesi�n de un texto, pero a nivel m�s particular o local. 
 													Esta distinci�n fue realizada por Teun van Dijk en 1980. </p>
											</div>
									</div>
								</li>
							</ul>
								</div>
							</div>
						</div> 
					</div>
				</div>
	
	
								
	</main>
	<!-- FOOTER -->



	


</body>
</html>