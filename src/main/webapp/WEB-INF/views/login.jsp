<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<!-- resources2 -->
		<%@ include file="/resources/templates/fixed_resources.jsp"%>
		
		<!-- TITLE -->
		<title>${contextTitle }</title>
	</head>
	
	<body>
		<!-- VALIDATIONS LOGIN -->
		<c:if test="${usuarioAutenticado}">
			<sec:authorize access="hasRole('ROLE_ADMINISTRADOR')" >
				<c:redirect url="/admin/dashboard" />
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_MAESTRO')" >
				<c:redirect url="/maestro/dashboard" />
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_ALUMNO')" >
				<c:redirect url="/alumno/dashboard" />
			</sec:authorize>
			
		</c:if>
		
		<!-- HEARDER -->
		<%@ include file="/resources/templates/header.jsp"%>
		
		<!-- MAIN -->
		<main>
		
		
			
			
			<div class="container">
				<div class="row">
					<div class="card col s12 m10 l8 xl6 offset-m1 offset-l2 offset-xl3 " >
						<div class="card-content center">
							<ul class="tabs" id="tabs-swipe-account">
								<li class="tab"><a class="active" href="#tab-login">Iniciar sesi�n</a></li>
								<li class="tab"><a class="active" href="#tab-consulta">Registrarse</a></li>
								
							</ul>
							<div id="tab-login">
								<div class="row">
									<c:url var="loginUrl" value="/api/authenticate" />
									<form action="${loginUrl}" method="post" id="formLogin">
										<br><br>
										<div class="row">
											<div class="input-field col s10 m6 l6 offset-s1 offset-m3 offset-l3">
												<input id="username" name="username" type="text" class="validate" maxlength="50" autofocus="autofocus" 	>
												<label for="username">Clave</label>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s10 m6 l6 offset-s1 offset-m3 offset-l3">
                                                <input id="password" name="password" type="password" class="validate" maxlength="30">
												<label for="password">Contrase�a</label>
											</div>
										</div>
										<div class="row">
											<div class="center">
												
												<button class="btn-large waves-effect waves-light btnAceptar pulse" id="btnLogin">�Entrar!<i class="material-icons right">send</i></button>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div id="tab-consulta">
								<div class="row col l12" >
									<form id="formRegistro">
										<br><br>
										<div class="row">
											<div class="input-field col l4  ">
												<input id="correoCon" name="correoCon" type="text" class="validate" maxlength="50" 	>
												<label for="correoCon">Correo</label>
											</div>
				
									
											<div class="input-field col l4 ">
                                                <input id="passwordCon" name="passwordCon" type="password" class="validate"  maxlength="30">
							 					<label for="passwordCon">Contrase�a</label>
											</div>
			
											<div class="input-field col l4 ">
                                                <input id="password2Con" name="password2Con" type="password" class="validate"  maxlength="30">
												<label for="password2Con">Confirma Contrase�a</label>
											</div>
											</div>
											<div class ="row">
											<div class="input-field col l4">
												<input id="nombreCon" name="nombreCon" type="text" class="validate" maxlength="50" 	>
												<label for="nombreCon">Nombre</label>
											</div>
											<div class="input-field col l4">
												<input id="apellidoPCon" name="apellidoPCon" type="text" class="validate" maxlength="50" 	>
												<label for="apellidoPCon">Apellido Paterno</label>
											</div>
											<div class="input-field col l4">
												<input id="apellidoMCon" name="apellidoMCon" type="text" class="validate" maxlength="50" 	>
												<label for="apellidoMCon">Apellido Materno</label>
											</div>
										</div>
										<div class="row">
											<div class="center col l12">
											
											
											<br>
											<br>
												<button class="btn-large waves-effect waves-light btnAceptar pulse" id="btnRegistro">�Registrar!<i class="material-icons right">send</i></button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
		
		<!-- FOOTER -->
		<%@ include file="/resources/templates/footer.jsp"%>
		
		<!-- GET MESSAGE -->
		<c:if test="${param.logout != null}">
			<c:set scope="request" var="userMessage" value="Has cerrado sesi�n exitosamente."></c:set>
		</c:if>
		
		<!-- GLOBAL VARIABLES JAVASCRIPT -->
		<script type="text/javascript">
			var MENSAJE_USUARIO = '${userMessage}'; 
		</script>
		
		<!-- MINE JAVASCRIPTS FILES -->
		<script charset="utf-8" type="text/javascript" src="<c:url value="/resources/js/validations/login.js" />"></script>
		<script charset="utf-8" type="text/javascript" src="<c:url value="/resources/js/login.js" />"></script>
		<script charset="utf-8" type="text/javascript" src="<c:url value="/resources/js/validations/registro.js" />"></script>
		<script charset="utf-8" type="text/javascript" src="<c:url value="/resources/js/registro.js" />"></script>
	</body>
</html>