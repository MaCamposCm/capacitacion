package uaq.mx.uaqvig.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MacroRemplazo {

	NOMBRE_USUARIO("{{nombreCompleto}}"),
	CAMPUS("{{campus}}"),
	ADSCRIPCION("{{adscripcion}}"),
	ENLACE("{{enlace}}"),
	FECHA("{{fecha}}"),
	LINK_SISTEMA("{{link}}"),
	ANIO_ACTUAL("{{fechaFooter}}");
	
	private String macro;
}
