package uaq.mx.uaqvig.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EnumEstatusNotificacion {
	
	ENVIADO(1L, "ENVIADO"),
	ERROR(2L, "ERROR"),
	POR_ENVIAR(3L, "POR_ENVIAR"),
	ENVIANDO(4L, "ENVIANDO");
	
	private Long idEstatus;
	private String estatus;
}
