package uaq.mx.uaqvig.enums;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EnumEstatusCaso {
	
	EN_CAPTURA(1L,"EN CAPTURA"),
	EN_PROCESO(2L,"EN PROCESO"),
	EN_INVESTIGACION(3L,"EN INVESTIGACION"),
	FINALIZADO(4L,"FINALIZADO"),
	CAPTURADO(5L,"CAPTURADO");
	
	private Long idEstatus;
	private String estatus;
	
	public static String getEstatusPorId(Long idEstatus) {
		if(idEstatus == 1L) {
			return EN_CAPTURA.estatus;
		}else if(idEstatus == 2L) {
			return EN_PROCESO.estatus;
		}else 	if(idEstatus == 3L) {
			return EN_INVESTIGACION.estatus;
		}else 	if(idEstatus == 4L) {
			return FINALIZADO.estatus;
		}else   if(idEstatus == 5L) {
				return CAPTURADO.estatus;
		}else {
			return null;
		}
	}
	
	public static List<String> getAllEstatus(){
		List<String> listEstatus = new ArrayList<>();
		listEstatus.add(EN_CAPTURA.getEstatus());
		listEstatus.add(EN_PROCESO.getEstatus());
		listEstatus.add(EN_INVESTIGACION.getEstatus());
		listEstatus.add(FINALIZADO.getEstatus());
		listEstatus.add(CAPTURADO.getEstatus());
		return listEstatus;
	}

}
