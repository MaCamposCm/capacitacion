package uaq.mx.uaqvig.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EnumEventoNotificacion {

	REGISTRO(1L,"NOTIFICACION_REGISTRO"),
	REGISTRO_RIESGO(2L,"NOTIFICACION_REGISTRO_ALTO_RIESGO");
	
	private Long id;
	private String descripcion;
}
