package mx.uaq.uavig.security.authentication;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import mx.uaq.uavig.service.NipAuthentication;



public class AuthenticationProvider  extends AbstractUserDetailsAuthenticationProvider{

//	@Autowired private UsuarioBusiness usuarioBusiness;
	@Autowired private UserDetailsService userDetailsService;
	@Autowired private NipAuthentication nipAuthenticationService;
	
	@Override
	protected void  additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

		
		Boolean responseNipAuthentication = nipAuthenticationService.validaCredenciales(
				authentication.getPrincipal().toString(),
				authentication.getCredentials().toString());

		
		if(!responseNipAuthentication) {

			System.out.println("ValidaNip : "+responseNipAuthentication);
			throw new BadCredentialsException("Authentication service reply: "+responseNipAuthentication);
		}
	}
		
	@Override
	protected UserDetails retrieveUser(String username, 
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		return userDetailsService.loadUserByUsername(username);
	}
	
	
	@SuppressWarnings("unused")
	private String getFolio(final HttpServletRequest request) {
		String folio = request.getParameter("folio");

		if(folio!=null)
			return folio;
		
		
		throw new BadCredentialsException("No se encuentran datos para el folio");
	}
}
