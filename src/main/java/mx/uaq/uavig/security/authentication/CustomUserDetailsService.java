package mx.uaq.uavig.security.authentication;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import mx.uaq.uavig.dao.UsuarioDAO;
import mx.uaq.uavig.model.Rol;
import mx.uaq.uavig.model.Usuario;

@Transactional
public class CustomUserDetailsService implements UserDetailsService {
	
@Autowired private UsuarioDAO usuarioDAO;

	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	 Usuario usuario = usuarioDAO.findOneByClave(username);
	 System.out.println(username+"este es el isiario");
		if(usuario == null)
			throw new UsernameNotFoundException("Usuario no encontrado " + username);
		
		System.out.println("Este es el usuario:"+usuario);
		UserDetails userDetails = createUserDetails(username, usuario, createGrantedAuthorities(usuario));
		
		return userDetails;
	}
	
	private List<GrantedAuthority> createGrantedAuthorities(final Usuario userFromDB) {
		List<GrantedAuthority> authorities = new ArrayList<>();
//		System.out.println("roles;"+userFromDB+"Y este es el usuario");
		
		for(Rol rol : userFromDB.getRoles())
			authorities.add(new SimpleGrantedAuthority(rol.getTipoRol()));
//		System.out.println(userFromDB.getRoles());
		return authorities;
	}
	
	private UserDetails createUserDetails(final String login, final Usuario userFromDB,
			List<GrantedAuthority> authorities) {
		CustomUserDetails uaqUserDetails;
		boolean accountEnabled = true;
//		EnumUsuarioEstatus estatus = EnumUsuarioEstatus.valueOf(userFromDB.getEstatus());
//		
//		if(!(estatus.equals(EnumUsuarioEstatus.BLOQUEADO)))
			uaqUserDetails = new CustomUserDetails(login + " " + userFromDB.getClave(), "noPassword", accountEnabled, true, authorities);
//		else
//			uaqUserDetails = new CustomUserDetails(login,  "", true, false, authorities);
			
		uaqUserDetails.setIdUsuario(Long.valueOf(userFromDB.getIdUsuario()));
		return uaqUserDetails;
	}

}
