package mx.uaq.uavig.filter;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CasoQuejaFilter extends AbstractSearchFilter{

	private String idCaso;
	private String folio;
	@DateTimeFormat(pattern = "dd/MM/yyyy")	
	private Date fechaRegistro;
	private String idAdscripcion;
	private String campus;
	private String enlace;
	private String estatus;
	
	
	
}
