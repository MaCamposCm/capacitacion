package mx.uaq.uavig.dao.security;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import mx.uaq.uavig.dao.UsuarioDAO;
import mx.uaq.uavig.model.Usuario;

@Repository
@Profile({"prod"})
public class NipAuthenticationDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired private UsuarioDAO usuarioDAO;


	
	public Boolean validaNipUsuario(final String clave, final String nip) {
		Usuario usuario = usuarioDAO.findOneByClave(clave);

	if(usuario!=null) {

			
				if(consultaValidaNip(clave,nip,"A")) {
					return true;	
				}else if(consultaValidaNip(clave,nip,"D")) {
					return true;
				}
			
		
	}			
	return false;
	
	}
	
	
	private Boolean consultaValidaNip(final String expediente, final String nip, final String tipoUsuario) {

		final String tipoUsuarioValida = tipoUsuario;

		String result = jdbcTemplate.execute(new CallableStatementCreator() {

			@Override
			public CallableStatement createCallableStatement(Connection con) throws SQLException {
				CallableStatement cs = con.prepareCall("{? = call VALIDANIP(?, ?, ?)}");
				cs.registerOutParameter(1, Types.VARCHAR);
				cs.setString(2, expediente);
				cs.setString(3, nip);
				cs.setString(4, tipoUsuarioValida);
				return cs;
			}
		}, new CallableStatementCallback<String>() {

			@Override
			public String doInCallableStatement(CallableStatement cs) throws SQLException, DataAccessException {
				cs.execute();
				String result = cs.getString(1);
				return result;
			}
		});
		return result.toLowerCase().equalsIgnoreCase("ok") ? true : false;
	}

	public Boolean consultaUsuarioInterno(final String nip, final String passUsu ) {
		if(nip.equals(passUsu)) {
			return true;
		}		
	return	false;
	}

}
