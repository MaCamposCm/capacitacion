package mx.uaq.uavig.dao;

import java.math.BigDecimal;


import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import mx.uaq.uavig.model.Materia;

@Repository
public class MateriaDAO extends AbstractjpaDao<Materia>{
	public MateriaDAO() {
		super(Materia.class);
		
	}
	
	
	public Long countMateriaByFilter() {
		try {
			String sqlQuery = "SELECT COUN(ID_MATERIA)+1"
					+ "FROM MATERIA";
			Query query = this.getEntityManager().createNativeQuery(sqlQuery);
				return ((BigDecimal) query.getSingleResult()).longValue();
			
		}catch(NoResultException e){
			return null;
		}
		
	}
	
	
		
	}
	

