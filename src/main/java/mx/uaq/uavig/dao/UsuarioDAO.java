package mx.uaq.uavig.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import mx.uaq.uavig.filter.UsuarioFilter;
import mx.uaq.uavig.model.Usuario;
import mx.uaq.uavig.model.UsuarioVista;

@Repository
public class UsuarioDAO extends AbstractjpaDao<Usuario> {
	public UsuarioDAO() {
		super(Usuario.class);
	}

	public Usuario findOneByClave(String clave) {
		try {
			final TypedQuery<Usuario> typedQuery = this.getEntityManager().createNamedQuery("Usuario.findByClave", Usuario.class);
			typedQuery.setParameter("clave", clave);			
			return (Usuario) typedQuery.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Long countUsuariosByFilter() {
		try {
			String sqlQuery =  "SELECT COUNT(ID_USUARIO)+1 "
				+ "FROM USUARIO ";
			Query query = this.getEntityManager().createNativeQuery(sqlQuery);
			
			return ((BigDecimal) query.getSingleResult()).longValue();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> getUsuariosByFilter(UsuarioFilter filter) {
		try {
			String sqlQuery = ""
				+ "SELECT * "
				+ "FROM USUARIO "
				+ filterUsuarios(filter, false);
			Query query = this.getEntityManager().createNativeQuery(sqlQuery, Usuario.class);
			query.setFirstResult(filter.getOffset());
			query.setMaxResults(filter.getLimit());
			
			return query.getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}
	

	public Long countUsuariosByFilter(UsuarioFilter filter) {
		try {
			String sqlQuery = ""
				+ "SELECT COUNT(ID_USUARIO) "
				+ "FROM USUARIO "
				+ filterUsuarios(filter, true);
			Query query = this.getEntityManager().createNativeQuery(sqlQuery);
			
			return ((BigDecimal) query.getSingleResult()).longValue();
		} catch(NoResultException e) {
			return null;
		}
	}
	
	private String filterUsuarios(UsuarioFilter filter, boolean isCount) {
		StringBuilder where = new StringBuilder();
		
		if(StringUtils.isNotBlank(filter.getClave()))
			where.append(" WHERE CLAVE = '" + filter.getClave() + "' ");
		if(StringUtils.isNotBlank(filter.getNombre())) {
			if(StringUtils.isNotBlank(where))
				where.append(" AND NOMBRE LIKE '" + filter.getPattern(filter.getNombre()) + "' ");
			else
				where.append(" WHERE NOMBRE LIKE '" + filter.getPattern(filter.getNombre()) + "' ");
		}
		if(StringUtils.isNotBlank(filter.getIdAdscripcion())) {
			if(StringUtils.isNotBlank(where))
				where.append(" AND ID_ADSCRIPCION = '" + filter.getIdAdscripcion() + "' ");
			else
				where.append(" WHERE ID_ADSCRIPCION = '" + filter.getIdAdscripcion() + "' ");
		}
		if(StringUtils.isNotBlank(filter.getTipo())) {
			if(StringUtils.isNotBlank(where))
				where.append(" AND TIPO_USUARIO = '" + filter.getTipo() + "' ");
			else
				where.append(" WHERE TIPO_USUARIO = '" + filter.getTipo() + "' ");
		}
		if(StringUtils.isNotBlank(filter.getEstatus())) {
			if(StringUtils.isNotBlank(where))
				where.append(" AND ESTATUS = '" + filter.getEstatus() + "' ");
			else
				where.append(" WHERE ESTATUS = '" + filter.getEstatus() + "' ");
		}
//		if(filter.getIdRol() != null) {
//			if(StringUtils.isNotBlank(where))
//				where.append(" AND ID IN (SELECT ID_USUARIO FROM ROL_USUARIO WHERE ID_ROL = " + filter.getIdRol() + ") ");
//			else
//				where.append(" WHERE ID IN (SELECT ID_USUARIO FROM ROL_USUARIO WHERE ID_ROL = " + filter.getIdRol() + ") ");
//		}
		if(StringUtils.isNotBlank(filter.getIdRol() )) {
			if(StringUtils.isNotBlank(where))
				where.append(" AND ID_USUARIO IN (SELECT ID_USUARIO FROM ROL_USUARIO WHERE ID_ROL = " + filter.getIdRol() + ") ");
			else
				where.append(" WHERE ID_USUARIO IN (SELECT ID_USUARIO FROM ROL_USUARIO WHERE ID_ROL = " + filter.getIdRol() + ") ");
		}
		
		if(!isCount)
			where.append(" ORDER BY CLAVE ");
		
		return where.toString();
	}

}
