package mx.uaq.uavig.business;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import mx.uaq.uavig.dao.RolUsuarioDAO;
import mx.uaq.uavig.dao.UsuarioDAO;
import mx.uaq.uavig.model.RolUsuario;
import mx.uaq.uavig.model.Usuario;
import mx.uaq.uavig.model.vo.FormDatosUsuario;

@Component
@Transactional

public class UsuarioBusiness {
  
	@Autowired UsuarioDAO usuarioDAO;
	@Autowired RolUsuarioDAO rolUsuarioDAO;
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Usuario registroUsuario(FormDatosUsuario datos)  {
		// TODO Auto-generated method stub
		try {
			if(datos!=null) {
				Usuario usu = new Usuario();
				//condicion ? true : false;s
				
				usu.setCorreo(datos.getCorreoCon());
				usu.setContrasena(datos.getPasswordCon());
				usu.setNombre(datos.getNombreCon());
				usu.setApellidoPaterno(datos.getApellidoPCon());
				usu.setApellidoMaterno(datos.getApellidoMCon());
				usu.setNombreCompleto((StringUtils.isNotBlank(datos.getApellidoMCon())) 
						?datos.getNombreCon() + " " + datos.getApellidoPCon() + " " + datos.getApellidoMCon()
						:datos.getNombreCon() + " " + datos.getApellidoPCon()
						);
				usuarioDAO.create(usu);
				createClave(usu);
				
				
				RolUsuario rolUsu = new RolUsuario(Long.parseLong("2"),usu.getIdUsuario());
				rolUsuarioDAO.create(rolUsu);
				
				return usu;
				
				
			}else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
		
	}


	private void createClave (Usuario usu) throws Exception {
		String id = String.valueOf(usu.getIdUsuario());
		StringBuilder idAux = new StringBuilder();
		StringBuilder clave = new StringBuilder("EMP-");
		
				
		for(int i = 1; i <5; i++ ) {
			if((idAux.length() + id.length())<5)
				idAux.append("0");
		}
		
		idAux.append(id);
		clave.append(idAux);
		usu.setClave(clave.toString());
		usuarioDAO.update(usu);
		
	}
	
}
