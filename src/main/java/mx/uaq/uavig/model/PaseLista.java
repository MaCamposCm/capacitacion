package mx.uaq.uavig.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name = "PASE_LISTA")

public class PaseLista implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@Column(name = "ID_PALIS")
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PASE_LISTA")
	@SequenceGenerator(name = "SEQ_PASE_LISTA", sequenceName = "SEQ_PASE_LISTA", allocationSize = 1)
	private Long idPalis;
	 


	@Id
	@NotNull
	@Column(name = "ID_MATERIA")
	@Basic(optional = false)
	private Long idMateria;
	
	@JoinColumns({
		@JoinColumn(name = "ID_MATERIA", referencedColumnName = "ID_MATERIA", insertable = false, updatable = false, nullable = false),
		@JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO", insertable = false, updatable = false, nullable = false)
	})
	@ManyToOne(fetch = FetchType.EAGER)
	private Materia materia;
	
	
	@Id 
	@NotNull
	@Column(name = "ID_ALUMNO")
	@Basic(optional = false)
	private Long idAlumno;
	 
	@JoinColumns({
		@JoinColumn(name = "ID_ALUMNO", referencedColumnName = "ID_ALUMNO", insertable = false, updatable = false, nullable = false),
		@JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO", insertable = false, updatable = false, nullable = false)
	})
	@ManyToOne(fetch = FetchType.EAGER)
	private Alumno alumno;
	
	
	@Basic (optional = false)
	@Column (name = "FECHA")
	private String fecha;
	
	@Basic (optional = false)
	@Column (name = "ASISTIO")
	private String asistio;
	
	
	
	
		
	}
	
