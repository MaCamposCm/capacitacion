package mx.uaq.uavig.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name="ALUMNO")
public class Alumno  implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@Basic(optional = false)
	@Column(name = "ID_ALUMNO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ALUMNO")
	@SequenceGenerator(name = "SEQ_ALUMNO", sequenceName = "SEQ_ALUMNO", allocationSize = 1)
	private Long idAlumno;
	
	@Basic (optional = false)
	@Column (name = "GRUPO")
	private String grupo;
	
	@Basic (optional = false)
	@Column (name = "SEMESTRE")
	private String semestre;
	
	@Id
	@NotNull
	@Column(name = "ID_USUARIO")
	@Basic(optional = false)
	private Long idUsuario;
	 
	@JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO", insertable = false, updatable = false, nullable = false)
	@ManyToOne(fetch = FetchType.EAGER)
	private Usuario usuario;

}
