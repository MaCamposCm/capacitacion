package mx.uaq.uavig.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name="MATERIA")

public class Materia implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@Basic(optional = false)
	@Column(name = "ID_MATERIA")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MATERIA")
	@SequenceGenerator(name = "SEQ_MATERIA", sequenceName = "SEQ_MATERIA", allocationSize = 1)
	private Long idMateria;
	
	@Basic(optional = false)
	@Column(name= "NOMBRE_MATERIA")
	private String nomMateria;
	
	@Basic (optional = false)
	@Column (name = "NUMERO_CAPITULOS")
	private String numCapitulos;
	
	@Id
	@NotNull
	@Column(name = "ID_USUARIO")
	@Basic(optional = false)
	private Long idUsuario;
	 
	@JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO", insertable = false, updatable = false, nullable = false)
	@ManyToOne(fetch = FetchType.EAGER)
	private Usuario usuario;
//	
//	public Materia(MateriaVO materiaVO) {
//		this.nomMateria = materiaVO.getNomMateria();
//		this.numCapitulos =materiaVO.getNumCapitulos();
//		this.grupo = materiaVO.getGrupo(); 
//		this.semestre = materiaVO.getSemestre();
//		this.idUsuario = materiaVO.getIdUsuario();
//		
//		
//	}
//	
//	public Materia(Materia materia) {
//		this.nomMateria = materia.getNomMateria();
//		this.numCapitulos = materia.getNumCapitulos();
//		this.grupo = materia.getGrupo();
//		this.semestre = materia.getSemestre();
//		this.idUsuario = materia.getIdUsuario();
//	}
	
}
