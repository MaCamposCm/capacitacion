package mx.uaq.uavig.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name = "V_PERSONAL_ADSCOR")
public class UsuarioVista implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@Basic(optional = false)
	@Column(name = "CLAVE")
	private String clave;

	@Basic(optional = false)
	@Column(name = "CORREO_UAQ")
	private String correoUaq;

	@Basic(optional = false)
	@Column(name = "NOMBRE")
	private String nombre;

	@Basic(optional = false)
	@Column(name = "APELLIDO_PATERNO")
	private String apellidoPaterno;

	@Basic(optional = false)
	@Column(name = "APELLIDO_MATERNO")
	private String apellidoMaterno;

	@Basic(optional = false)
	@Column(name = "TELEFONO")
	private String telefono;

	@Basic(optional = false)
	@Column(name = "ADSCRIP")
	private String idAdscripcion;
	
	@Basic(optional = false)
	@Column(name = "DESCRIP_ADSCRIP")
	private String descripAdscrip;



	@Basic(optional = false)
	@Column(name = "NOMBRE_COMPLETO")
	private String nombreCompleto;
	
}
