package mx.uaq.uavig.model.vo;

import lombok.Data;

@Data
public class MateriaVO {
 	private Long idMateria;
	private String nomMateria;
	private String numCapitulos;
	private String grupo;
	private String semestre; 
	private Long idUsuario;
 	

}
