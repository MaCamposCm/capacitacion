package mx.uaq.uavig.model.vo;

import java.util.List;

import lombok.Data;

@Data
public class PaseListaVO {

	private Long idPalis;
	private List<Long> idMateria;
	private List<Long> idUsuario;
	private String fecha;
	private String asistio;
}

