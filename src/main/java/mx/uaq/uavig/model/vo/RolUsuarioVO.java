package mx.uaq.uavig.model.vo;

import lombok.Data;

@Data
public class RolUsuarioVO {

	private Long idRol;
	private Long idUsuario;
	
}
