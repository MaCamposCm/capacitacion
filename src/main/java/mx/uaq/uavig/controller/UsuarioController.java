package mx.uaq.uavig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.uaq.uavig.business.UsuarioBusiness;
import mx.uaq.uavig.exception.UaqException;
import mx.uaq.uavig.model.Usuario;
import mx.uaq.uavig.model.vo.FormDatosUsuario;
import uaq.mx.uaqvig.enums.EnumMensajeError;

@RestController
public class UsuarioController {
	@Autowired private UsuarioBusiness usuarioBusiness;
	
	
	
	@ResponseBody
	@RequestMapping(value= {"registroUsuario"},produces="application/json; charset=utf-8",method=RequestMethod.POST)
	public Usuario registroUsuario(@ModelAttribute FormDatosUsuario datos) {
		try {
			return usuarioBusiness.registroUsuario(datos);
		} catch (Throwable e) {
		 throw new UaqException(EnumMensajeError.USUARIO_ACTIVAR,e);
		}
		
		
	}

	
	
	
	
	
}
